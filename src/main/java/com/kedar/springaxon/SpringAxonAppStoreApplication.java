package com.kedar.springaxon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAxonAppStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAxonAppStoreApplication.class, args);
	}

}
