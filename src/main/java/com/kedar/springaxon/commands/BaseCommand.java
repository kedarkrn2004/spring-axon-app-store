package com.kedar.springaxon.commands;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

public class BaseCommand<T> {

    @TargetAggregateIdentifier
    private final T id;

    public BaseCommand(final T id) {
        this.id = id;
    }

    public T getId() {
        return id;
    }
}
