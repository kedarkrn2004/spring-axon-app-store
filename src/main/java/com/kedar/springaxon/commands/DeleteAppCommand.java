package com.kedar.springaxon.commands;

public class DeleteAppCommand extends BaseCommand<String> {

    public DeleteAppCommand(String id) {
        super(id);
    }
}
