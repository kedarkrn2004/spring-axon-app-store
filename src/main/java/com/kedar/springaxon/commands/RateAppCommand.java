package com.kedar.springaxon.commands;

public class RateAppCommand extends BaseCommand<String> {

    private final float ratings;

    public RateAppCommand(String id, float ratings) {
        super(id);
        this.ratings = ratings;
    }

    public float getRatings() {
        return ratings;
    }
}
