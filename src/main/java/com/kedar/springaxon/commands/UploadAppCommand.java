package com.kedar.springaxon.commands;

public class UploadAppCommand extends BaseCommand<String> {

    private final String name;
    private final String description;

    public UploadAppCommand(String id, String name, String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
