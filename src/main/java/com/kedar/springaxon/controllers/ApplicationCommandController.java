package com.kedar.springaxon.controllers;

import java.util.concurrent.CompletableFuture;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kedar.springaxon.service.ApplicationCommandService;
import com.kedar.springaxon.service.model.RateAppDTO;
import com.kedar.springaxon.service.model.UploadAppDTO;

@RestController
@RequestMapping("/apps")
public class ApplicationCommandController {

    private final ApplicationCommandService applicationCommandService;

    public ApplicationCommandController(ApplicationCommandService applicationCommandService) {
        this.applicationCommandService = applicationCommandService;
    }

    @PostMapping("/upload")
    public CompletableFuture<String> upload(@RequestBody UploadAppDTO uploadAppDto) {
        return applicationCommandService.uploadApplication(uploadAppDto);
    }

    @PostMapping("/download/{appNumber}")
    public CompletableFuture<String> download(@PathVariable String appNumber) {
        return applicationCommandService.downloadApplication(appNumber);
    }

    @DeleteMapping("/{appNumber}")
    public CompletableFuture<String> delete(@PathVariable String appNumber) {
        return applicationCommandService.deleteApplication(appNumber);
    }

    @PostMapping("/ratings")
    public CompletableFuture<String> rateApp(@RequestBody RateAppDTO rateAppDto) {
        return applicationCommandService.rateApplication(rateAppDto);
    }

}
