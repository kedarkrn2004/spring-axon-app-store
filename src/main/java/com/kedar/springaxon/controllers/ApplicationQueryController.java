package com.kedar.springaxon.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kedar.springaxon.service.ApplicationQueryService;

@RestController
@RequestMapping("/apps")
public class ApplicationQueryController {

    private final ApplicationQueryService applicationQueryService;

    public ApplicationQueryController(ApplicationQueryService applicationQueryService) {
        this.applicationQueryService = applicationQueryService;
    }

    @GetMapping("/events/{appNumber}")
    public List<Object> listAppEvents(@PathVariable String appNumber) {
        return applicationQueryService.listApplicationEvents(appNumber);
    }
}
