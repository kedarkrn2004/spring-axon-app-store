package com.kedar.springaxon.entities;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import com.kedar.springaxon.commands.DeleteAppCommand;
import com.kedar.springaxon.commands.DownloadAppCommand;
import com.kedar.springaxon.commands.RateAppCommand;
import com.kedar.springaxon.commands.UploadAppCommand;
import com.kedar.springaxon.events.AppDeletedEvent;
import com.kedar.springaxon.events.AppDownloadedEvent;
import com.kedar.springaxon.events.AppRatedEvent;
import com.kedar.springaxon.events.AppUploadedEvent;

@Aggregate
public class Application {

    @AggregateIdentifier
    private String id;

    private String name;
    private String description;
    private float ratings;
    private int totalDownloads = 0;

    public Application() {

    }

    @CommandHandler
    public Application(UploadAppCommand cmd) {
        apply(new AppUploadedEvent(cmd.getId(), cmd.getName(), cmd.getDescription()));
    }

    @EventSourcingHandler
    public void on(AppUploadedEvent evnt) {
        this.id = evnt.getId();
        this.name = evnt.getName();
        this.description = evnt.getDescription();

    }

    @CommandHandler
    public void handleDownloadAppCommand(DownloadAppCommand cmd) {
        apply(new AppDownloadedEvent(cmd.getId()));
    }

    @EventSourcingHandler
    public void handleAppDownloadedEvent(AppDownloadedEvent evnt) {
        this.totalDownloads += 1;
    }

    @CommandHandler
    public void handleDeleteAppCommand(DeleteAppCommand cmd) {
        apply(new AppDeletedEvent(cmd.getId()));
    }

    @EventSourcingHandler
    public void handleAppDeletedEvent(AppDeletedEvent evnt) {
        this.totalDownloads -= 1;
    }

    @CommandHandler
    public void handleRateAppCommand(RateAppCommand cmd) {
        apply(new RateAppCommand(cmd.getId(), cmd.getRatings()));
    }

    @EventSourcingHandler
    public void handleAppRatedEvent(AppRatedEvent evnt) {
        this.ratings = evnt.getRatings();
    }
}
