package com.kedar.springaxon.events;

public class AppDeletedEvent extends BaseEvent<String> {

    public AppDeletedEvent(String id) {
        super(id);
    }
}
