package com.kedar.springaxon.events;

public class AppDownloadedEvent extends BaseEvent<String> {

    public AppDownloadedEvent(String id) {
        super(id);
    }
}
