package com.kedar.springaxon.events;

public class AppRatedEvent extends BaseEvent<String> {

    private final float ratings;

    public AppRatedEvent(String id, float ratings) {
        super(id);
        this.ratings = ratings;
    }

    public float getRatings() {
        return ratings;
    }
}
