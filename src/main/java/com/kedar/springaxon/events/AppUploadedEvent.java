package com.kedar.springaxon.events;

public class AppUploadedEvent extends BaseEvent<String> {

    private final String name;
    private final String description;

    public AppUploadedEvent(String id, String name, String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
