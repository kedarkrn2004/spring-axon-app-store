package com.kedar.springaxon.service;

import java.util.concurrent.CompletableFuture;

import com.kedar.springaxon.service.model.RateAppDTO;
import com.kedar.springaxon.service.model.UploadAppDTO;

public interface ApplicationCommandService {

    CompletableFuture<String> uploadApplication(UploadAppDTO uploadAppDto);

    CompletableFuture<String> downloadApplication(String appNumber);

    CompletableFuture<String> deleteApplication(String appNumber);

    CompletableFuture<String> rateApplication(RateAppDTO rateAppDto);
}
