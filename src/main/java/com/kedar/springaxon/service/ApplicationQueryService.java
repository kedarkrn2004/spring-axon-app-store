package com.kedar.springaxon.service;

import java.util.List;

public interface ApplicationQueryService {

    List<Object> listApplicationEvents(String appNumber);

}
