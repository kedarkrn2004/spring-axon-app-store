package com.kedar.springaxon.service.impl;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;

import com.kedar.springaxon.commands.DeleteAppCommand;
import com.kedar.springaxon.commands.DownloadAppCommand;
import com.kedar.springaxon.commands.RateAppCommand;
import com.kedar.springaxon.commands.UploadAppCommand;
import com.kedar.springaxon.service.ApplicationCommandService;
import com.kedar.springaxon.service.model.RateAppDTO;
import com.kedar.springaxon.service.model.UploadAppDTO;

@Service
public class ApplicationCommandServiceImpl implements ApplicationCommandService {

    private final CommandGateway commandGateway;

    public ApplicationCommandServiceImpl(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public CompletableFuture<String> uploadApplication(UploadAppDTO uploadAppDto) {
        return commandGateway.send(new UploadAppCommand(UUID.randomUUID().toString(), uploadAppDto.getName(),
                uploadAppDto.getDescription()));
    }

    @Override
    public CompletableFuture<String> downloadApplication(String appNumber) {
        return commandGateway.send(new DownloadAppCommand(appNumber));
    }

    @Override
    public CompletableFuture<String> deleteApplication(String appNumber) {
        return commandGateway.send(new DeleteAppCommand(appNumber));
    }

    @Override
    public CompletableFuture<String> rateApplication(RateAppDTO rateAppDto) {
        return commandGateway.send(new RateAppCommand(rateAppDto.getAppNumber(), rateAppDto.getRatings()));
    }

}
