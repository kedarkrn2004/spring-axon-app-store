package com.kedar.springaxon.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.stereotype.Service;

import com.kedar.springaxon.service.ApplicationQueryService;

@Service
public class ApplicationQueryServiceImpl implements ApplicationQueryService {

    private final EventStore eventStore;

    public ApplicationQueryServiceImpl(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    @Override
    public List<Object> listApplicationEvents(String appNumber) {
        return eventStore.readEvents(appNumber).asStream().map(m -> m.getPayload()).collect(Collectors.toList());
    }
}
