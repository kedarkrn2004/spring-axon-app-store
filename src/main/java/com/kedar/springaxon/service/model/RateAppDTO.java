package com.kedar.springaxon.service.model;

public class RateAppDTO {

    private final String appNumber;
    private final float ratings;

    public RateAppDTO(String appNumber, float ratings) {
        super();
        this.appNumber = appNumber;
        this.ratings = ratings;
    }

    public String getAppNumber() {
        return appNumber;
    }

    public float getRatings() {
        return ratings;
    }
}
