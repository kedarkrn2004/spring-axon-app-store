package com.kedar.springaxon.service.model;

public class UploadAppDTO {

    private final String name;
    private final String description;

    public UploadAppDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

}
